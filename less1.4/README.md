# Less 1.4
A dockerfile for nginx 1.19.1 + lua module 0.10.15 (build from sources).
## Usage example
```sh
$ wget https://gitlab.com/albaslug/juneway-homework/-/raw/master/less1.4/Dockerfile
$ docker build -t nginx-lua:v1.0 .
$ wget https://trac.nginx.org/nginx/export/HEAD/nginx/conf/nginx.conf
$ docker run -d -p 8080:80 -v $(pwd)/nginx.conf:/opt/nginx/conf/nginx.conf nginx-lua:v1.0
```
