# Less 1.5
Docker-in-Docker variation based on Debian 10. Just like the original *dind*, needs to be run with the **--privileged** key.
## Startup example
```sh
$ wget https://gitlab.com/albaslug/juneway-homework/-/raw/master/less1.5/Dockerfile
$ docker build -t my-dind:1.0 .
$ docker run -d --privileged --name dindtest my-dind:1.0

```
