## Less 1.1: Django + Nginx + Postgres


Requirements:
- docker
- docker-compose


Usage:

- download this folder
- docker-compose up --build --detach
